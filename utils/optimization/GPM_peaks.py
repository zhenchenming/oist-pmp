import numpy as np
from datetime import datetime
from dataclasses import dataclass
from quantum.quantum_operations import project_ket,expect_value
from optimization.metrics import fraction_yield,quantum_yield,integrated_yield,ensemble_yields

def dS(t,t0):
    return 1/(1 + (t - t0)**2*10000**2)*2/np.pi

def S(t,t0):
    return np.arctan((t-t0)*10000)*2/np.pi


@dataclass
class GPM:
    H : object
    ode : object
    ensemble : list
    
    def gradient_field(self,lambda_):
        d_ux = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode.w_t,self.H.op.op['S_x'],self.ode.y_t).imag
        d_uy = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode.w_t,self.H.op.op['S_y'],self.ode.y_t).imag
        d_uz = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode.w_t,self.H.op.op['S_z'],self.ode.y_t).imag
        return np.array([d_ux,d_uy,d_uz]).T

    def gradient_hyperfine(self,lambda_):
        d_A = np.zeros(self.H.A.shape)
        if self.H.anisotropy:
            for i in range(self.H.n_nuclei):
                tiknov = lambda_*self.H.A[:,i].sum(axis=(1,2))
                for j,d_i in enumerate(['x','y','z']):
                        for k,d_s in enumerate(['x','y','z']):
                            d_A[:,i,j,k] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%i%s'%(i+1,d_i)].dot(self.H.op.op['S_1%s'%d_s]),self.ode.y_t).imag
        else:
            for i in range(self.H.n_nuclei):
                tiknov = lambda_*self.H.A[:,i].sum(axis=(1,2))
                d_A[:,i,0,0] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%ix'%(i+1)].dot(self.H.op.op['S_1x']),self.ode.y_t).imag
                d_A[:,i,1,1] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%iy'%(i+1)].dot(self.H.op.op['S_1y']),self.ode.y_t).imag
                d_A[:,i,2,2] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%iz'%(i+1)].dot(self.H.op.op['S_1z']),self.ode.y_t).imag
        return d_A
    
    def field_optimization(self,lr_u,lambda_u,epochs,tol,update_lr=False,bounds=(-100,100),maximize=False):
        J = np.zeros((epochs+1)) #include initial yield
        g_u = np.zeros(self.H.omega_t.shape)
        #get parameters for each control
        m_u,M_u = bounds

        for i in range(epochs):
            g_u *= 0
            #solve ode for each psi_0
            for j,psi_0 in enumerate(self.ensemble):
                self.ode.set_y(psi_0)
                self.ode.solve_forward('rk4')
                self.ode.solve_backward('rk4')
                #calculate gradient
                g_u += self.gradient_field(lambda_u)/self.H.Z
                J[i] += quantum_yield(self.H,self.ode.y_t)/self.H.Z
            #gradient descent
            for j in range(3):
                for k in range(0,len(self.t0),2):
                    g_t = dS(self.H.t,self.t0[k,j])*(-S(self.H.t,self.t0[k+1,j]))*g_u[:,j]
                    self.t0[k,j] += (-1)**(maximize)*lr_u*g_t.sum()*(M_u-m_u)
                    g_t = -S(self.H.t,self.t0[k,j])*dS(self.H.t,self.t0[k+1,j])*g_u[:,j]
                    self.t0[k+1,j] += (-1)**(maximize)*lr_u*g_t.sum()*(M_u-m_u)
                    #update field
                    new = np.sum([S(self.H.t,self.t0[l,j])*(-S(self.H.t,self.t0[l+1,j]))+1 for l in range(0,len(self.t0),2)],axis=0)
                    self.H.omega_t[:,j] = new*(M_u-m_u)+m_u
            #restrict field to bounds
            self.H.omega_t = np.clip(self.H.omega_t,m_u,M_u)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            time = datetime.now()
            if i%10==0:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute))
            else:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute),end='\r')

        #calculate final yield
        f_y,i_y,J[-1] = ensemble_yields(self.ensemble,self.ode,self.H,method='rk4')
        return J,f_y,i_y,g_u


    def hyperfine_optimization(self,lr_A,lambda_u,epochs,tol,update_lr=False,bounds=(-100,100),maximize=False):
        #self.t0 = np.linspace(0,self.H.t[-1],24)
        #self.t0 = np.repeat(self.t0[:,None],3,axis=1)
        J = np.zeros((epochs+1)) #include initial yield
        g_A = np.zeros(self.H.A.shape)
        #get parameters for each control
        m_u,M_u = bounds

        for i in range(epochs):
            g_A *= 0
            #solve ode for each psi_0
            for j,psi_0 in enumerate(self.ensemble):
                self.ode.set_y(psi_0)
                self.ode.solve_forward('rk4')
                self.ode.solve_backward('rk4')
                #calculate gradient
                #g_u += self.gradient_field(lambda_u)/self.H.Z
                g_A += self.gradient_hyperfine(lambda_u)/self.H.Z
                J[i] += quantum_yield(self.H,self.ode.y_t)/self.H.Z
            #gradient descent
            for j in range(3):
                for k in range(0,len(self.t0),2):
                    g_t = dS(self.H.t,self.t0[k,j])*(-S(self.H.t,self.t0[k+1,j]))*g_A[:,0,j,j]
                    self.t0[k,j] += (-1)**(maximize)*lr_A*g_t.sum()*(M_u-m_u)
                    g_t = -S(self.H.t,self.t0[k,j])*dS(self.H.t,self.t0[k+1,j])*g_A[:,0,j,j]
                    self.t0[k+1,j] += (-1)**(maximize)* lr_A*g_t.sum()*(M_u-m_u)
                    #update field
                    new = np.sum([S(self.H.t,self.t0[l,j])*(-S(self.H.t,self.t0[l+1,j]))+1 for l in range(0,len(self.t0),2)],axis=0)
                    #self.H.omega_t[:,j] = new*(M_u-m_u)+m_u
                    self.H.A[:,0,j,j] = new*(M_u-m_u)/2+m_u
            #restrict field to bounds
            #self.H.omega_t = np.clip(self.H.omega_t,m_u,M_u)
            self.H.A = np.clip(self.H.A,m_u,M_u)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            time = datetime.now()
            if i%10==0:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute))
            else:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute),end='\r')

        #calculate final yield
        f_y,i_y,J[-1],j2 = ensemble_yields(self.ensemble,self.ode,self.H,method='rk4')
        return J,f_y,i_y,g_A

    def optimization(self,lr,epochs,tol,bounds=((-100,100),(-100,100)),maximize=False):
        J = np.zeros((epochs+1))
        g_A = np.zeros(self.H.A.shape)
        g_u = np.zeros(self.H.omega_t.shape)
        (m_u,M_u),(m_A,M_A) = bounds
        lr_u,lr_A = lr
        
        for i in range(epochs):
            g_A *= 0
            g_u *= 0
            #solve ode for each psi_0
            for j,psi_0 in enumerate(self.ensemble):
                self.ode.set_y(psi_0)
                self.ode.solve_forward('rk4')
                self.ode.solve_backward('rk4')
                #calculate gradient
                g_u += self.gradient_field(0)/self.H.Z
                g_A += self.gradient_hyperfine(0)/self.H.Z
                J[i] += quantum_yield(self.H,self.ode.y_t)/self.H.Z
            #gradient descent
            for j in range(3):
                for k in range(0,len(self.t0),2):
                    g_t = dS(self.H.t,self.t0[k,j])*(-S(self.H.t,self.t0[k+1,j]))*g_A[:,0,j,j]
                    self.t0[k,j] += (-1)**(maximize)*lr_A*g_t.sum()*(M_A-m_A)
                    g_t = -S(self.H.t,self.t0[k,j])*dS(self.H.t,self.t0[k+1,j])*g_A[:,0,j,j]
                    self.t0[k+1,j] += (-1)**(maximize)* lr_A*g_t.sum()*(M_A-m_A)
                    #update field
                    new = np.sum([S(self.H.t,self.t0[l,j])*(-S(self.H.t,self.t0[l+1,j]))+1 for l in range(0,len(self.t0),2)],axis=0)
                    self.H.A[:,0,j,j] = new*(M_A-m_A)+m_A
                for k in range(0,len(self.t0_u),2):
                    g_t = dS(self.H.t,self.t0_u[k,j])*(-S(self.H.t,self.t0_u[k+1,j]))*g_u[:,j]
                    self.t0_u[k,j] += (-1)**(maximize)*lr_u*g_t.sum()*(M_u-m_u)
                    g_t = -S(self.H.t,self.t0_u[k,j])*dS(self.H.t,self.t0_u[k+1,j])*g_u[:,j]
                    self.t0_u[k+1,j] += (-1)**(maximize)* lr_u*g_t.sum()*(M_u-m_u)
                    #update field
                    new = np.sum([S(self.H.t,self.t0_u[l,j])*(-S(self.H.t,self.t0_u[l+1,j]))+1 for l in range(0,len(self.t0_u),2)],axis=0)
                    self.H.omega_t[:,j] = new*(M_u-m_u)+m_u
            #restrict field to bounds
            self.H.omega_t = np.clip(self.H.omega_t,m_u,M_u)
            self.H.A = np.clip(self.H.A,m_A,M_A)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            time = datetime.now()
            if i%10==0:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute))
            else:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute),end='\r')