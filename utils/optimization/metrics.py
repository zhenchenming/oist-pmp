import numpy as np
from quantum.quantum_operations import expect_value
from hamiltonian.projectors import singProj,tripProj
from hamiltonian.kron import kron_

def fraction_yield(H,psi):
    return 0.5*H.k_S* expect_value(psi, H.O, psi).real*H.gamma_e

def quantum_yield(H,psi):
    return np.trapz(fraction_yield(H,psi), x=H.t/H.gamma_e)

def integrated_yield(H,psi):
    i_y = np.zeros((H.n_time))
    f_y = fraction_yield(H,psi)
    for i in range(1,H.n_time):
        i_y[i] = np.trapz(f_y[:i], x=H.t[:i]/H.gamma_e)
    return i_y

def J2(H,psi):
    Ps = kron_((singProj(sparse=False),np.eye(H.Z))).astype(complex)
    f_y = 0.5*H.k_S* expect_value(psi, Ps, psi).real*H.gamma_e
    return np.trapz(f_y, x=H.t/H.gamma_e)

def ensemble_yields(ensemble,ode,H,method='lbv5',build_Ht=True):
    #calculate fraction and integrated yield
    f_y = 0
    i_y = 0
    q_y = 0
    j_2 = 0
    for j,psi in enumerate(ensemble):
            ode.set_y(psi)
            ode.solve_forward(method=method,build_Ht=build_Ht)
            f_y+=fraction_yield(H,ode.y_t)/H.Z
            i_y+=integrated_yield(H,ode.y_t)/H.Z
            q_y+=quantum_yield(H,ode.y_t)/H.Z
            j_2+=J2(H,ode.y_t)/H.Z
    return f_y,i_y,q_y,j_2