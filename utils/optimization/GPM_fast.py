import numpy as np
from datetime import datetime
from dataclasses import dataclass
from quantum.quantum_operations import project_ket,expect_value
from optimization.metrics import fraction_yield,quantum_yield,integrated_yield,ensemble_yields
# --------------------------------- temporal --------------------------------- #
from hamiltonian.projectors import singProj,tripProj
from hamiltonian.kron import kron_
# --------------------------------- temporal --------------------------------- #

def J2(H,psi):
    Ps = kron_((singProj(sparse=False),np.eye(H.Z))).astype(complex)
    f_y = 0.5*H.k_S* expect_value(psi, Ps, psi).real*H.gamma_e
    return np.trapz(f_y, x=H.t/H.gamma_e)


@dataclass
class GPM:
    H : object
    ode : object
    ode_2 : object
    ensemble : list
    
    def gradient_field(self,lambda_):
        d_ux = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode_2.w_t,self.H.op.op['S_x'],self.ode_2.y_t).imag
        d_uy = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode_2.w_t,self.H.op.op['S_y'],self.ode_2.y_t).imag
        d_uz = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode_2.w_t,self.H.op.op['S_z'],self.ode_2.y_t).imag
        return np.array([d_ux,d_uy,d_uz]).T
    
    def gradient_hyperfine(self,lambda_):
        d_A = np.zeros(self.H.A.shape)
        if self.H.anisotropy:
            for i in range(self.H.n_nuclei):
                tiknov = lambda_*self.H.A[:,i].sum(axis=(1,2))
                for j,d_i in enumerate(['x','y','z']):
                        for k,d_s in enumerate(['x','y','z']):
                            d_A[:,i,j,k] = tiknov +2* expect_value(self.ode_2.w_t,self.H.op.op['I_%i%s'%(i+1,d_i)].dot(self.H.op.op['S_1%s'%d_s]),self.ode_2.y_t).imag
        else:
            for i in range(self.H.n_nuclei):
                tiknov = lambda_*self.H.A[:,i].sum(axis=(1,2))
                d_A[:,i,0,0] = tiknov +2* expect_value(self.ode_2.w_t,self.H.op.op['I_%ix'%(i+1)].dot(self.H.op.op['S_1x']),self.ode_2.y_t).imag
                d_A[:,i,1,1] = tiknov +2* expect_value(self.ode_2.w_t,self.H.op.op['I_%iy'%(i+1)].dot(self.H.op.op['S_1y']),self.ode_2.y_t).imag
                d_A[:,i,2,2] = tiknov +2* expect_value(self.ode_2.w_t,self.H.op.op['I_%iz'%(i+1)].dot(self.H.op.op['S_1z']),self.ode_2.y_t).imag
        return d_A

    def field_optimization(self,lr,lambda_,epochs,tol,update_lr=False,bounds=(-100,100),maximize=False):
        J,f_y,i_y,g_u,_ = self.optimization((lr,0),(lambda_,0),epochs,tol,update_lr,(bounds,(-100,100)),maximize)
        return J,f_y,i_y,g_u
    def hyperfine_optimization(self,lr,lambda_,epochs,tol,update_lr=False,bounds=(-100,100),maximize=False):
        J,f_y,i_y,_,g_A = self.optimization((0,lr),(0,lambda_),epochs,tol,update_lr,((-100,100),bounds),maximize)
        return J,f_y,i_y,g_A
    def optimization(self,lrs,lambdas,epochs,tol,update_lr=False,bounds=((-100,100),(-100,100)),maximize=False):
        J = np.zeros((epochs+1)) #include initial yield
        g_u = np.zeros(self.H.omega_t.shape)
        g_A = np.zeros(self.H.A.shape)
        #get parameters for each control
        lr_u,lr_A = lrs
        lambda_u,lambda_A = lambdas
        (m_u,M_u),(m_A,M_A) = bounds

        for i in range(epochs):
            #update learning rate if needed
            if update_lr and i and not i%update_lr:
                if lr_A==0:
                    lr_u = self.update_lr_u(lambda_u,(m_u,M_u))
                elif lr_u==0:
                    lr_A = self.update_lr_A(lambda_A,(m_A,M_A))
                else:
                    lr_u,lr_A = self.update_lr(lambda_u,lambda_A,bounds)
            #reset gradient and yield
            g_u *= 0
            g_A *= 0
            J_2 = 0
            #solve ode for each psi_0

            if i%10==0:
                y = []
                w = []
                k=0
                for psi_0 in self.ensemble:
                    print(k,len(self.ensemble))
                    self.ode.set_y(psi_0)
                    self.ode.solve_forward('rk4')
                    self.ode.solve_backward('rk4')
                    y.append(self.ode.y_t)
                    w.append(self.ode.w_t)
                    k+=1


            for j,psi_0 in enumerate(self.ensemble):
                print(j,len(self.ensemble))
                self.ode_2.y_t = y[j]
                self.ode_2.w_t = w[j]
                self.ode_2.solve_forward()
                self.ode_2.solve_backward()
                y[j] = self.ode_2.y_t.copy()
                w[j] = self.ode_2.w_t.copy()
                #calculate gradient
                if not np.isscalar(lr_u) or lr_u>0:
                    g_u += self.gradient_field(lambda_u)/self.H.Z
                if not np.isscalar(lr_A) or lr_A>0:
                    g_A += self.gradient_hyperfine(lambda_A)/self.H.Z
                J[i] += quantum_yield(self.H,self.ode_2.y_t)/self.H.Z
                J_2 += J2(self.H,self.ode_2.y_t)/self.H.Z
            #gradient descent
            if not np.isscalar(lr_u) or lr_u>0:
                if lr_u>1.e3 and i>0:
                    self.H.omega_t[abs(g_u)>1.e-4] -= (-1)**(maximize)*lr_u*g_u[abs(g_u)>1.e-4]
                else:        
                    self.H.omega_t -= (-1)**(maximize)*lr_u*g_u
                #restrict field to bounds
                self.H.omega_t = np.clip(self.H.omega_t,m_u,M_u)
            if not np.isscalar(lr_A) or lr_A>0:
                self.H.A -= (-1)**(maximize)*lr_A*g_A
                #restrict hyperfine to bounds
                self.H.A =np.clip(self.H.A,m_A,M_A)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            time = datetime.now()

            if i%100==0:
                print('Epoch: %i/%i, J: %.8e, J2: %.8e, Time: %i:%i'%(i,epochs,J[i],J_2,time.hour,time.minute))
            else:
                print('Epoch: %i/%i, J: %.8e, J2: %.8e, Time: %i:%i'%(i,epochs,J[i],J_2,time.hour,time.minute),end='\r')

        #calculate final yield
        f_y,i_y,J[-1],j2 = ensemble_yields(self.ensemble,self.ode,self.H,method='rk4')
        return J,f_y,i_y,g_u,g_A

    def update_lr_u(self,lambda_,bounds):
        print('Starting lr search')
        field = self.H.omega_t.copy()
        lrs = np.logspace(-2,0,3)
        lr = np.argmax( [ #min if minimization
            self.field_optimization(l,lambda_,1,0,bounds=bounds)[0][-1]
            for l in lrs
        ])
        lr=lrs[lr]
        self.H.omega_t = field
        print('lr search ended')
        return lr
    
    def update_lr_A(self,lambda_,bounds):
        print('Starting lr search\n')
        A = self.H.A.copy()
        lrs = np.logspace(-3,-1,7)
        lr = np.argmax( [ #min if minimization
            self.hyperfine_optimization(l,lambda_,2,0,bounds=bounds)[0][-1]
            for l in lrs
        ])
        lr=lrs[lr]
        self.H.A = A
        print('lr search ended\n')
        return lr
    
    def update_lr(self,lambdas,bounds):
        lrs_u = np.logspace(-2,0,3)
        lrs_A = np.logspace(-2,0,3)
        combinations = np.array(np.meshgrid(lrs_u,lrs_A)).T.reshape(-1,2)
        lr = np.argmax( [ #min if minimization
            self.optimization(l,lambdas,1,0,bounds=bounds)[0][-1]
            for l in combinations
        ])