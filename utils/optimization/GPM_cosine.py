import numpy as np
from datetime import datetime
from dataclasses import dataclass
from quantum.quantum_operations import project_ket,expect_value
from optimization.metrics import fraction_yield,quantum_yield,integrated_yield,ensemble_yields

def dS(t,t0=0):
    return 1/(1 + (t - t0)**2*1.e4**2)*2/np.pi

def S(t,t0=0):
    return np.arctan((t-t0)*1.e4)*2/np.pi


@dataclass
class GPM:
    H : object
    ode : object
    ensemble : list
    
    def gradient_field(self,lambda_):
        d_ux = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode.w_t,self.H.op.op['S_x'],self.ode.y_t).imag
        d_uy = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode.w_t,self.H.op.op['S_y'],self.ode.y_t).imag
        d_uz = lambda_*self.H.omega_t.sum(axis=1)+ 2* expect_value(self.ode.w_t,self.H.op.op['S_z'],self.ode.y_t).imag
        return np.array([d_ux,d_uy,d_uz]).T

    def gradient_hyperfine(self,lambda_):
        d_A = np.zeros(self.H.A.shape)
        if self.H.anisotropy:
            for i in range(self.H.n_nuclei):
                tiknov = lambda_*self.H.A[:,i].sum(axis=(1,2))
                for j,d_i in enumerate(['x','y','z']):
                        for k,d_s in enumerate(['x','y','z']):
                            d_A[:,i,j,k] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%i%s'%(i+1,d_i)].dot(self.H.op.op['S_1%s'%d_s]),self.ode.y_t).imag
        else:
            for i in range(self.H.n_nuclei):
                tiknov = lambda_*self.H.A[:,i].sum(axis=(1,2))
                d_A[:,i,0,0] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%ix'%(i+1)].dot(self.H.op.op['S_1x']),self.ode.y_t).imag
                d_A[:,i,1,1] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%iy'%(i+1)].dot(self.H.op.op['S_1y']),self.ode.y_t).imag
                d_A[:,i,2,2] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%iz'%(i+1)].dot(self.H.op.op['S_1z']),self.ode.y_t).imag
        return d_A
    
    def field_optimization(self,lr_u,lambda_u,epochs,tol,update_lr=False,bounds=(-100,100),maximize=False):
        J = np.zeros((epochs+1)) #include initial yield
        g_A = np.zeros(self.H.A.shape)
        #get parameters for each control
        m_u,M_u = bounds

        for i in range(epochs):
            g_A *= 0
            #solve ode for each psi_0
            for j,psi_0 in enumerate(self.ensemble):
                self.ode.set_y(psi_0)
                self.ode.solve_forward('rk4')
                self.ode.solve_backward('rk4')
                #calculate gradient
                g_u += self.gradient_field(lambda_u)/self.H.Z
                J[i] += quantum_yield(self.H,self.ode.y_t)/self.H.Z
            #gradient descent
            for j in range(3):
                for k in range(0,len(self.t0),2):
                    g_t = dS(self.H.t,self.t0[k,j])*(-S(self.H.t,self.t0[k+1,j]))*g_u[:,j]
                    self.t0[k,j] += lr_u*g_t.sum()*(M_u-m_u)
                    g_t = -S(self.H.t,self.t0[k,j])*dS(self.H.t,self.t0[k+1,j])*g_u[:,j]
                    self.t0[k+1,j] += lr_u*g_t.sum()*(M_u-m_u)
                    #update field
                    new = np.sum([S(self.H.t,self.t0[l,j])*(-S(self.H.t,self.t0[l+1,j]))+1 for l in range(0,len(self.t0),2)],axis=0)
                    self.H.omega_t[:,j] = new*(M_u-m_u)+m_u
            #restrict field to bounds
            self.H.omega_t = np.clip(self.H.omega_t,m_u,M_u)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            time = datetime.now()
            if i%10==0:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute))
            else:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute),end='\r')

        #calculate final yield
        f_y,i_y,J[-1] = ensemble_yields(self.ensemble,self.ode,self.H,method='rk4')
        return J,f_y,i_y,g_u


    def hyperfine_optimization(self,lr_A,lambda_u,epochs,tol,update_lr=False,bounds=(-100,100),maximize=False):
        J = np.zeros((epochs+1)) #include initial yield
        g_A = np.zeros(self.H.A.shape)
        #get parameters for each control
        m_u,M_u = bounds

        for i in range(epochs):
            g_A *= 0
            #solve ode for each psi_0
            if i%300==0:
                y = np.zeros((len(self.ensemble)*2*self.ode.y_t.shape[1]),dtype=complex)

            for j,psi_0 in enumerate(self.ensemble):
                self.ode.set_y(psi_0)
                self.ode.solve_forward('rk4')
                self.ode.solve_backward('rk4')
                #calculate gradient
                #g_u += self.gradient_field(lambda_u)/self.H.Z
                g_A += self.gradient_hyperfine(lambda_u)/self.H.Z
                J[i] += quantum_yield(self.H,self.ode.y_t)/self.H.Z

                if i%300==0:
                    y[j*self.ode.y_t.shape[1]:(j+1)*self.ode.y_t.shape[1]] = self.ode.y_t[0]
                    y[y.shape[0]//2+j*self.ode.y_t.shape[1]:y.shape[0]//2+(j+1)*self.ode.y_t.shape[1]] = self.ode.w_t[0]


            if i%300==0:
                aux = self.H.A.copy()
                aux_2 = self.H.omega_t.copy()
                sol = self.ode.solve_bvp(((-100,100),bounds),y)
                self.H.A = aux
                self.H.omega_t = aux_2
                print('\n-------------\nX_bvp(T):%.8e\n-------------\n'%(np.linalg.norm(sol[-1,sol.shape[1]//2:])))

            if i==0:
                idx = [300,300,300]#[300,300,300]
            #get first sign change of gradient
            
            #gradient descent
            g_w = 0
            g_c = 0
            for j in range(3):
                g_w += dS(np.cos(self.w*self.H.t+self.phi[j])+self.c)*np.sin(self.w*self.H.t+self.phi[j])*self.H.t*g_A[:,0,j,j]
                g_phi = dS(np.cos(self.w*self.H.t+self.phi[j])+self.c)*np.sin(self.w*self.H.t+self.phi[j])*g_A[:,0,j,j]
                g_c += (-1)*dS(np.cos(self.w*self.H.t+self.phi[j])+self.c)*g_A[:,0,j,j]        

                self.phi[j] += (-1)**(maximize)*lr_A*g_phi[idx[j]:].sum()*(M_u-m_u)/2*1.e4

                #self.c[j] += (-1)**(maximize)*lr_A*g_c[idx[j]:].sum()*(M_u-m_u)/2*1.e3    
            
            self.w += (-1)**(maximize)*lr_A*g_w[idx[j]:].sum()*(M_u-m_u)/2
            self.c += (-1)**(maximize)*lr_A*g_c[idx[j]:].sum()*(M_u-m_u)/2*1.e3

                #print(abs(d_w-self.w[j]),abs(d_phi-self.phi[j]),abs(d_c-self.c[j]))
            for j in range(3):
                self.H.A[idx[j]:,0,j,j] = S(np.cos(self.w*self.H.t[idx[j]:]+self.phi[j])+self.c)*(M_u-m_u)/2+(M_u+m_u)/2

            #restrict field to bounds
            #self.H.omega_t = np.clip(self.H.omega_t,m_u,M_u)
            self.H.A = np.clip(self.H.A,m_u,M_u)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            time = datetime.now()
            if i%10==0:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute))
                print('--------')
                print(self.phi)
                print('--------')
                print(self.c)
            else:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute),end='\r')

        #calculate final yield
        f_y,i_y,J[-1] = ensemble_yields(self.ensemble,self.ode,self.H,method='rk4')
        return J,f_y,i_y,g_A