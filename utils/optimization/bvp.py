from typing import Any
import numpy as np
from dataclasses import dataclass
from quantum.quantum_operations import project_ket,expect_value

@dataclass
class BVP:
    H : object
    bounds : tuple
    def __call__(self,t,y):
        y1_t = y[None,:y.shape[0]//4]
        y2_t = y[None,y.shape[0]//4:y.shape[0]//2]
        w1_t = y[None,y.shape[0]//2:3*y.shape[0]//4]
        w2_t = y[None,3*y.shape[0]//4:]
        # ------------------------------ field gradient ------------------------------ #
        g_u = []
        for coord in ['x','y','z']:
            g_u.append(2* expect_value(w1_t,self.H.op.op['S_%s'%coord],y1_t).imag)
            g_u[-1] += 2* expect_value(w2_t,self.H.op.op['S_%s'%coord],y2_t).imag
        
        
        
        # ------------------------------ hyperfine gradient ------------------------------ #
        g_A = []
        for i in range(self.H.n_nuclei):
            for j,d_i in enumerate(['x','y','z']):
                g_A.append(2* expect_value(w1_t,self.H.op.op['I_%i%s'%(i+1,d_i)].dot(self.H.op.op['S_1%s'%(d_i)]),y1_t).imag)
                g_A[-1] += 2* expect_value(w2_t,self.H.op.op['I_%i%s'%(i+1,d_i)].dot(self.H.op.op['S_1%s'%(d_i)]),y2_t).imag

        # ------------------- calculate hamiltonian at given point ------------------- #
        idx = t/self.H.dt+1.e-6
        bounds_u,bounds_A = self.bounds

        for j in range(3):
            #self.H.A[int(idx),0,j,j] = np.sign(g_A[j])*(bounds_A[1]-bounds_A[0])/2+(bounds_A[0]+bounds_A[1])/2
            self.H.omega_t[int(idx),j] = np.sign(g_u[j])*(bounds_u[1]-bounds_u[0])/2+(bounds_u[0]+bounds_u[1])/2

        outp = np.zeros(y.shape,dtype=complex)

        outp[:y.shape[0]//4] = ((-1j)*self.H(int(idx)).dot(y1_t[0])).flatten()
        outp[y.shape[0]//4:y.shape[0]//2] = ((-1j)*self.H(int(idx)).dot(y2_t[0])).flatten()
        outp[y.shape[0]//2:3*y.shape[0]//4] = ((-1j)*(self.H(int(idx))+2.0j*self.H.K).dot(w1_t[0]) -0.5*self.H.k_S * project_ket(self.H.O,y1_t[0])).flatten()
        outp[3*y.shape[0]//4:] = ((-1j)*(self.H(int(idx))+2.0j*self.H.K).dot(w2_t[0]) -0.5*self.H.k_S * project_ket(self.H.O,y2_t[0])).flatten()

        if abs(idx-int(idx))<2.e-6:
            pass
        else:
            for j in range(3):
                #self.H.A[int(idx+1),0,j,j] = np.sign(g_A[j])*(bounds_A[1]-bounds_A[0])/2+(bounds_A[0]+bounds_A[1])/2
                self.H.omega_t[int(idx+1),j] = np.sign(g_u[j])*(bounds_u[1]-bounds_u[0])/2+(bounds_u[0]+bounds_u[1])/2
            
            outp[:y.shape[0]//4] += (-1j)*self.H(int(idx+1)).dot(y1_t[0])
            outp[y.shape[0]//4:y.shape[0]//2] += (-1j)*self.H(int(idx+1)).dot(y2_t[0])
            outp[y.shape[0]//2:3*y.shape[0]//4] += (-1j)*(self.H(int(idx+1))+2.0j*self.H.K).dot(w1_t[0]) -0.5*self.H.k_S * project_ket(self.H.O,y1_t[0])
            outp[3*y.shape[0]//4:] += (-1j)*(self.H(int(idx+1))+2.0j*self.H.K).dot(w2_t[0]) -0.5*self.H.k_S * project_ket(self.H.O,y2_t[0])
            outp/=2

        return outp

