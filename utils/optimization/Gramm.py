import numpy as np
from datetime import datetime
from dataclasses import dataclass
from quantum.quantum_operations import project_ket,expect_value
from optimization.metrics import fraction_yield,quantum_yield,integrated_yield,ensemble_yields

@dataclass
class Gramm:
    H : object
    ode : object
    ensemble : list
    
    def field_optimization(self,epochs,lambda_,tol,bounds=(-100,100),maximize=False):
        J = np.zeros((epochs+1)) #include initial yield
        m,M = bounds

        for i in range(epochs):
            A=0
            P=0
            #solve ode for each psi_0 and calculate A,P
            for j,psi_0 in enumerate(self.ensemble):
                self.ode.set_y(psi_0)
                self.ode.solve_forward('rk4')
                self.ode.solve_sensitivity_u('rk4')
                #A
                _A = np.einsum('ki,tij->tkj',self.H.O,self.ode.U_t)
                _A = np.einsum('tkl,tkj->tlj',self.ode.U_t.conj(),_A).real
                _A = np.trapz(_A,x=self.H.t,axis=0)
                A += _A
                #regularization
                _A += lambda_*np.eye(_A.shape[0])
                #P
                _P = np.einsum('ki,ti->tk',self.H.O,self.ode.y_t)
                _P = -np.einsum('tkl,tk->tl',self.ode.U_t.conj(),_P).real
                _P = np.trapz(_P,x=self.H.t,axis=0)
                P += _P
                #quantum yield
                J[i] += quantum_yield(self.H,self.ode.y_t)/self.H.Z

            du = np.linalg.solve(A,P)
            #update field
            self.H.omega_t -= (-1)**(maximize)*du
            #limitate bounds
            self.H.omega_t = np.clip(self.H.omega_t,m,M)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            time = datetime.now()
            if i%10==0:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute))
            else:
                print('Epoch: %i/%i, J: %.8e, Time: %i:%i'%(i,epochs,J[i],time.hour,time.minute),end='\r')

        #calculate final yield
        f_y,i_y,J[-1] = ensemble_yields(self.ensemble,self.ode,self.H,method='rk4')
        return J,f_y,i_y