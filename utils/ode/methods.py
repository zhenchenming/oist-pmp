import numpy as np
import mpmath
from scipy.linalg import expm
import sys

"""
def lobattoIIIA(f, t, h, y_0):
    # Compute the coefficients
    k1 = h * f(t, y_0)
    k2 = h * f(t + h/5, y_0 + k1/5)
    k3 = h * f(t + 3*h/5, y_0 + (k1 + 3*k2)/5)
    k4 = h * f(t + h, y_0 + (5*k1 + 8*k2 - k3)/10)
    k5 = h * f(t + 4*h/5, y_0 + (3*k1 - 9*k2 + 6*k3 + 9*k4)/5)
    
    return y_0 + (2*k1 + 7*k2 + 6*k3 + 2*k4 + k5)/18
"""

def lobattoIIIA(f, t, h, y_0):
    # Compute the coefficients
    A = np.array([[5/24, -1/24, -1/24],
                  [1/24, 5/24, -1/24],
                  [1/24, 1/24, 5/24]])
    b = np.array([1/6, 2/3, 1/6])
    
    k1 = f(t, y_0)
    k2 = f(t + h/2, y_0 + h/2 * k1)
    k3 = f(t+h, y_0 + h * (A[0,0] * k1 + A[0,1] * k2))
    y = y_0 + h * (b[0] * k1 + b[1] * k2 + b[2] * k3)
    
    return y


def lbv5(f,t,h,y_0):
    y = np.zeros((len(t),*y_0.shape)).astype(complex)
    if h>0:
        y[0] = y_0
        for i in range(1,len(t)):
            y[i] = lobattoIIIA(f,t[i-1],h,y[i-1])
    else:
        y[-1] = y_0
        for i in range(len(t)-2,-1,-1):
            y[i] = lobattoIIIA(f,t[i+1],h,y[i+1])
    return y


def runge_kutta_4(f,t,h,y_0):
    k1 = f(t,y_0)
    k2 = f(t+h/2,y_0+h*k1/2)
    k3 = f(t+h/2,y_0+h*k2/2)
    k4 = f(t+h,y_0+h*k3)
    return y_0 + h*(k1+2*k2+2*k3+k4)/6


def rk4(f,t,h,y_0):
    y = np.zeros((len(t),*y_0.shape)).astype(complex)
    if h>0:
        y[0] = y_0
        for i in range(1,len(t)):
            y[i] = runge_kutta_4(f,t[i-1],h,y[i-1])
    else:
        y[-1] = y_0
        for i in range(len(t)-2,-1,-1):
            y[i] = runge_kutta_4(f,t[i+1],h,y[i+1])
    return y


def propagateExpM(P,Q,y_0,dt):
    y_0 = np.array([mpmath.mpc(x_elem.real, x_elem.imag) for x_elem in y_0.flatten()]).reshape(y_0.shape)
    return expm(dt*P).dot(y_0)+Q*dt

def Linear_ODE(P,Q,t,h,y_0):
    y = np.zeros((len(t),y_0.shape[0])).astype(complex)
    if h>0:
        y[0] = y_0
        for i in range(1,len(t)):
            y[i] = propagateExpM(P(i-1),Q(i-1),y[i-1],h)
    else:
        y[-1] = y_0
        for i in range(len(t)-2,-1,-1):
            y[i] = propagateExpM(P(i+1),Q(i+1),y[i+1],h)
    return y


def shooting(f,y_0s):
    y_0,y_1 = y_0s
    s0 = f(y_0)
    s1 = f(y_1)

    iters = 0
    while np.linalg.norm(s0[-1])>1.e-3 and np.linalg.norm(s1[-1])>1.e-3 and iters<50:
        print(iters,end='\r')
        iters+=1

        denominator = (s1[-1]-s0[-1])/((y_1-y_0)+1.e-8)
        y = y_1 - s1[-1]/denominator
        y_0 = y_1
        y_1 = y
        s0 = s1
        s1 = f(y_1)
    return s1