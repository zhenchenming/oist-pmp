import numpy as np
from quantum.quantum_operations import project_ket
from ode.methods import rk4,lbv5,Linear_ODE
from scipy.sparse import csc_matrix

#propagateExpM_ = lambda P,Q,y_0,dt: propagateExpM(P,Q,y_0,dt,exponential=True,sparse=False)

class H_ODE:
    def __init__(self,H,y_0=None):
        self.H = H
        if y_0 is not None:
            self.set_y(y_0)
    def set_y(self,y_0):
        self.y_t = np.zeros((self.H.n_time, y_0.shape[0])).astype(complex)
        self.y_t[0] = y_0
        self.w_t = np.zeros((self.H.n_time, y_0.shape[0])).astype(complex)

    def solve_forward(self,build_Ht=True):
        if build_Ht:
            #Calculates H for all times before solving
            self.H.build_H_t()

        P = self.H.H_0[:-1]
        y = self.y_t[:-1]
        k1 = (-1j)*np.einsum('tij,tj->ti',P,y)
        P = (self.H.H_0[1:]+self.H.H_0[:-1])/2
        y = (self.y_t[1:]+self.y_t[:-1])/2+self.H.dt*k1/2
        k2 = (-1j)*np.einsum('tij,tj->ti',P,y)
        y = (self.y_t[1:]+self.y_t[:-1])/2+self.H.dt*k2/2
        k3 = (-1j)*np.einsum('tij,tj->ti',P,y)
        P = self.H.H_0[1:]
        y = self.y_t[1:]+self.H.dt*k3
        k4 = (-1j)*np.einsum('tij,tj->ti',P,y)
        
        self.y_t[1:]= self.y_t[1:]+ self.H.dt*(k1+2*k2+2*k3+k4)/6
    
    def solve_backward(self,build_Ht=True):
        Pb = (self.H.H_0+2.0j*self.H.K)[::-1]
        Qb = (-0.5*self.H.k_S*project_ket(self.H.O,self.y_t))[::-1]
        yb = self.w_t[::-1]

        P = Pb[:-1]
        y = yb[:-1]
        Q = Qb[:-1]
        
        k1 = (-1j)*np.einsum('tij,tj->ti',P,y)+Q
        
        P = (Pb[1:]+Pb[:-1])/2
        y = (yb[1:]+yb[:-1])/2+self.H.dt*k1/2
        Q = (Qb[1:]+Qb[:-1])/2
        
        k2 = (-1j)*np.einsum('tij,tj->ti',P,y)+Q
        y = (yb[1:]+yb[:-1])/2+self.H.dt*k2/2
        k3 = (-1j)*np.einsum('tij,tj->ti',P,y)+Q
        P = Pb[1:]
        Q = Qb[1:]
        y = yb[1:]+self.H.dt*k3
        k4 = (-1j)*np.einsum('tij,tj->ti',P,y)+Q

        self.w_t[:-1] = self.w_t[:-1]+(self.H.dt*(k1+2*k2+2*k3+k4)/6)[::-1]