import numpy as np
from dataclasses import dataclass
from quantum.quantum_operations import expect_value
from ode.methods import rk4

@dataclass
class BVP_J:
    H : object
    bounds : tuple
    e : float = 1e-13

    def __post_init__(self):
        self.H0 = 0
        for i in range(self.H.n_nuclei):
            self.H0 += self.H.A[:,i,0,0,None,None]*self.H.op.op['I_%ix'%(i+1)].dot(self.H.op.op['S_1x'])[None,:,:] \
                + self.H.A[:,i,1,1,None,None]*self.H.op.op['I_%iy'%(i+1)].dot(self.H.op.op['S_1y'])[None,:,:] \
                + self.H.A[:,i,2,2,None,None]*self.H.op.op['I_%iz'%(i+1)].dot(self.H.op.op['S_1z'])[None,:,:]
        
    
    def set_u0(self,u0):
        self.u[0] = u0 #TODO

    def calculate_J(self,ys):
        y1,y2 = ys #singlet states
        J1 = self.J(y1).real
        J2 = self.J(y2).real
        self.j = np.zeros((self.H.n_time,64,64),dtype=float)
        for i,j in np.ndindex((4,4)):
            self.j[:,(2*i)*8:(2*i+1)*8,(2*j)*8:(2*j+1)*8] = J1[i,j]
            self.j[:,(2*i+1)*8:(2*i+2)*8,(2*j+1)*8:(2*j+2)*8] = J2[i,j]
        return self.j

    def solve(self,ys):
        self.calculate_J(ys)

        u = np.zeros((self.H.n_time,64,32),dtype=float)
        u[0,32:] = np.eye(32)
        u = rk4(self.f,self.H.t,self.H.dt,u[0])
        return u


    def f(self,t,u):
        idx = (t/self.H.dt+1.e-6)
        if abs(idx-int(idx))<2.e-6:
            return self.j[int(idx)].dot(u)
        d = idx-int(idx)
        return ((self.j[int(idx)]+self.j[int(idx)+1])/2).dot(u)
        

    def J(self,y):
        #wave functions
        psi1,psi2,chi1,chi2 = y
        #Spin operators
        Sx = self.H.op.op['S_x'].real
        Sy = self.H.op.op['S_y'].imag
        Sz = self.H.op.op['S_z'].real
        #Pontryagin functions and derivatives
        gx,gy,gz = self.gradient_field(y)
        self.hx = self.h(gx)
        self.hy = self.h(gy)
        self.hz = self.h(gz)
        #derivatives of field control
        dp1 = np.einsum('ti,ij->tj',chi1,Sy)-np.einsum('ti,ij->tj',chi2,Sx+Sz)
        dp2 = np.einsum('ti,ij->tj',chi2,Sy)+np.einsum('ti,ij->tj',chi1,Sx+Sz)
        dx1 = np.einsum('ij,tj->ti',Sy,psi1)  +np.einsum('ij,tj->ti',(Sx+Sz),psi2)
        dx2 = np.einsum('ij,tj->ti',Sy,psi2)  -np.einsum('ij,tj->ti',(Sx+Sz),psi1)

        #Hamiltonian
        H = self.H0
        H += (self.bounds[0]-self.bounds[1])/2*self.molif(gx)[:,None,None]*self.H.op.op['S_x'][None,...]
        H += (self.bounds[0]-self.bounds[1])/2*self.molif(gy)[:,None,None]*self.H.op.op['S_y'][None,...]
        H += (self.bounds[0]-self.bounds[1])/2*self.molif(gz)[:,None,None]*self.H.op.op['S_z'][None,...]

        H1 = H.real
        H2 = H.imag -1.0j*self.H.K
        Hc1 = H1
        Hc2 = H.imag +1.0j*self.H.K

        # --------------------- calculate all partial derivatives -------------------- #
        #partial f1 / partial psi 1
        df1_dp1 = self.hx*np.einsum('tk,ij,tj->tki',dp1,Sx,psi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dp1,Sy,psi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dp1,Sz,psi2)+ \
        H2
        #partial f1 / partial psi 2
        df1_dp2 = self.hx*np.einsum('tk,ij,tj->tki',dp2,Sx,psi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dp2,Sy,psi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dp2,Sz,psi2)+ \
        H1
        #partial f1 / partial chi 1
        df1_dx1 = self.hx*np.einsum('tk,ij,tj->tki',dx1,Sx,psi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dx1,Sy,psi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dx1,Sz,psi2)
        #partial f1 / partial chi 2
        df1_dx2 = self.hx*np.einsum('tk,ij,tj->tki',dx2,Sx,psi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dx2,Sy,psi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dx2,Sz,psi2)
        #partial f2 / partial psi 1
        df2_dp1 = -self.hx*np.einsum('tk,ij,tj->tki',dp1,Sx,psi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dp1,Sy,psi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dp1,Sz,psi1)- \
        H1
        #partial f2 / partial psi 2
        df2_dp2 = -self.hx*np.einsum('tk,ij,tj->tki',dp2,Sx,psi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dp2,Sy,psi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dp2,Sz,psi1)- \
        H2
        #partial f2 / partial chi 1
        df2_dx1 = -self.hx*np.einsum('tk,ij,tj->tki',dx1,Sx,psi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dx1,Sy,psi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dx1,Sz,psi1)
        #partial f2 / partial chi 2
        df2_dx2 = -self.hx*np.einsum('tk,ij,tj->tki',dx2,Sx,psi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dx2,Sy,psi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dx2,Sz,psi1)
        #partial g1 / partial psi 1
        dg1_dp1 = self.hx*np.einsum('tk,ij,tj->tki',dp1,Sx,chi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dp1,Sy,chi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dp1,Sz,chi2)+ \
        self.H.k_S*self.H.O
        #partial g1 / partial psi 2
        dg1_dp2 = self.hx*np.einsum('tk,ij,tj->tki',dp2,Sx,chi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dp2,Sy,chi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dp2,Sz,chi2)
        #partial g1 / partial chi 1
        dg1_dx1 = self.hx*np.einsum('tk,ij,tj->tki',dx1,Sx,chi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dx1,Sy,chi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dx1,Sz,chi2)+ \
        Hc2
        #partial g1 / partial chi 2
        dg1_dx2 = self.hx*np.einsum('tk,ij,tj->tki',dx2,Sx,chi2)+ \
        self.hy*np.einsum('tk,ij,tj->tki',dx2,Sy,chi1)+ \
        self.hz*np.einsum('tk,ij,tj->tki',dx2,Sz,chi2)+ \
        Hc1
        #partial g2 / partial psi 1
        dg2_dp1 = -self.hx*np.einsum('tk,ij,tj->tki',dp1,Sx,chi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dp1,Sy,chi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dp1,Sz,chi1)
        #partial g2 / partial psi 2
        dg2_dp2 = -self.hx*np.einsum('tk,ij,tj->tki',dp2,Sx,chi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dp2,Sy,chi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dp2,Sz,chi1)+ \
        self.H.k_S*self.H.O
        #partial g2 / partial chi 1
        dg2_dx1 = -self.hx*np.einsum('tk,ij,tj->tki',dx1,Sx,chi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dx1,Sy,chi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dx1,Sz,chi1)- \
        Hc1
        #partial g2 / partial chi 2
        dg2_dx2 = -self.hx*np.einsum('tk,ij,tj->tki',dx2,Sx,chi1)- \
        self.hy*np.einsum('tk,ij,tj->tki',dx2,Sy,chi2)- \
        self.hz*np.einsum('tk,ij,tj->tki',dx2,Sz,chi1)- \
        Hc2

        J = np.array([
            [df1_dp1, df1_dp2, df1_dx1, df1_dx2],
            [df2_dp1, df2_dp2, df2_dx1, df2_dx2],
            [dg1_dp1, dg1_dp2, dg1_dx1, dg1_dx2],
            [dg2_dp1, dg2_dp2, dg2_dx1, dg2_dx2]
        ])
        return J
    
    def molif(self,x,n=1):
        #g = lambda x,e : np.exp(e**2/(x**2-e**2))
        #C = 4 - 4/np.e #int_-1^1 dx exp(1/(x^2-1))
        #int_approx = lambda h,e: np.trapz(g(np.linspace(h,self.e-1.e-12,1001),e),x=np.linspace(h,self.e,1001))
        
        #(1-2*C/self.e**n * int_approx(x,self.e))

        rsts = np.zeros(x.shape)
        rsts[x>self.e] = 1
        rsts[x<-self.e] = -1
        return rsts
    
    def h(self,x):
        return (self.bounds[1]-self.bounds[0])/2#*np.exp(self.e**2/(x**2-self.e**2))
    
    def gradient_field(self,y):
        psi1,psi2,chi1,chi2 = y
        psi = psi1+1j*psi2
        chi = chi1+1j*chi2
        d_ux =  2* expect_value(chi,self.H.op.op['S_x'],psi).imag
        d_uy =  2* expect_value(chi,self.H.op.op['S_y'],psi).imag
        d_uz =  2* expect_value(chi,self.H.op.op['S_z'],psi).imag
        return d_ux,d_uy,d_uz
    
    def calculate_h(self,y):
        gx,gy,gz = self.gradient_field(y)
        self.hx = self.h(gx)
        self.hy = self.h(gy)
        self.hz = self.h(gz)
