import numpy as np
from quantum.quantum_operations import project_ket
from ode.methods import rk4,lbv5,Linear_ODE
from optimization.bvp import BVP
from scipy.sparse import csc_matrix

#propagateExpM_ = lambda P,Q,y_0,dt: propagateExpM(P,Q,y_0,dt,exponential=True,sparse=False)

class H_ODE:
    def __init__(self,H,y_0=None):
        self.H = H
        if y_0 is not None:
            self.set_y(y_0)
    def set_y(self,y_0):
        self.y_t = np.zeros((self.H.n_time, y_0.shape[0])).astype(complex)
        self.y_t[0] = y_0
        self.w_t = np.zeros((self.H.n_time, y_0.shape[0])).astype(complex)

    def solve_forward(self,method,build_Ht=True): #TODO: add option to build in rk4
        if build_Ht:
            #Calculates H for all times before solving
            self.H.build_H_t()

        Aux = lambda idx,y,b: (-1j)*self.H.H_0[idx].dot(y) if b else (-1j)*self.H(idx).dot(y)
        P = lambda idx,y: Aux(idx,y,b=build_Ht)
        Q = lambda idx : 0
        if method=='rk4':
            f = lambda t,y : self.f(t,y,P,Q)
            self.y_t = rk4(f,self.H.t,self.H.dt,self.y_t[0])
        elif method=='lbv5':
            f = lambda t,y : self.f(t,y,P,Q)
            self.y_t = lbv5(f,self.H.t,self.H.dt,self.y_t[0])
        elif method=='Ht':
            P = lambda idx: Aux(idx,np.eye(self.y_t.shape[1]),b=build_Ht)
            Q = lambda idx: 0
            self.y_t = Linear_ODE(P,Q,self.H.t,self.H.dt,self.y_t[0])
        else:
            raise NotImplementedError
    
    def solve_backward(self,method,built_Ht=True):
        Aux = lambda idx,y,b: (-1j)*(self.H.H_0[idx]+ 2.0j*self.H.K).dot(y) if b else (-1j)*(self.H(idx).conj()).dot(y)
        P = lambda idx,y: Aux(idx,y,built_Ht)
        Q = lambda idx : -0.5*self.H.k_S * project_ket(self.H.O,self.y_t[idx])
        if method=='rk4':
            f = lambda t,y : self.f(t,y,P,Q)
            self.w_t = rk4(f,self.H.t,-self.H.dt,self.w_t[-1])
        elif method=='lbv5':
            f = lambda t,y : self.f(t,y,P,Q)
            self.w_t = lbv5(f,self.H.t,-self.H.dt,self.w_t[-1])
        elif method=='Ht':
            P = lambda idx: Aux(idx,np.eye(self.y_t.shape[1]),built_Ht)
            self.w_t = Linear_ODE(P,Q,self.H.t,-self.H.dt,self.w_t[-1])
        else:
            raise NotImplementedError
        
    def solve_bvp(self,bounds,y0):
        bvp = BVP(self.H,bounds)
        sol = rk4(bvp, self.H.t, self.H.dt, y0)
        return sol
    
    def solve_forward_chi(self,method,built_Ht=True):
        if method=='rk4':
            Aux = lambda idx,y,b: (-1j)*(self.H.H_0[idx]+ 2.0j*self.H.K).dot(y) if b else (-1j)*self.H(idx).dot(y)
            P = lambda idx,y: Aux(idx,y,built_Ht)
            Q = lambda idx : -0.5*self.H.k_S * project_ket(self.H.O,self.y_t[idx])
            f = lambda t,y : self.f(t,y,P,Q)
            self.w_t = rk4(f,self.H.t,self.H.dt,self.w_t[0])
        elif method=='Ht':
            P = lambda idx: self.P(idx,np.eye(self.w_t.shape[1]),built_H=built_Ht)
            self.w_t = Linear_ODE(P,self.Q,self.H.t,self.H.dt,self.w_t[0])
        else:
            raise NotImplementedError

    
    def solve_sensitivity_u(self,method,built_Ht=True):
        self.U_t = np.zeros((self.H.n_time,self.y_t.shape[1],3)).astype(complex)
        if method=='rk4':
            Aux = lambda idx,y,b: (-1j)*self.H.H_0[idx].dot(y) if b else (-1j)*self.H(idx).dot(y)
            P = lambda idx,y: Aux(idx,y,built_Ht)
            Q = lambda idx: np.array([self.H.op.op['S_%s'%(s)].dot(self.y_t[idx]) for s in ['x','y','z']]).T
            f = lambda t,y : self.f(t,y,P,Q)
            self.U_t = rk4(f,self.H.t,self.H.dt,self.U_t[0])

    def solve_sensitivity_chi(self,method,built_Ht=True):
        self.U_t = np.zeros((self.H.n_time,self.y_t.shape[1],self.y_t.shape[1])).astype(complex)
        self.U_t[0] = np.eye(self.y_t.shape[1])
        if method=='rk4':
            Aux = lambda idx,y,b: (-1j)*self.H.H_0[idx].dot(y) if b else (-1j)*self.H(idx).dot(y)
            P = lambda idx,y: Aux(idx,y,built_Ht)
            Q = lambda idx: 0
            f = lambda t,y : self.f(t,y,P,Q)
            self.U_t = rk4(f,self.H.t,self.H.dt,self.U_t[0])

    def solve_sensitivity_VBP(self):
        psi_r = np.zeros((self.H.n_time,self.y_t.shape[1]*2)).astype(float)
        psi_r[:,:self.y_t.shape[1]] = (self.y_t).real
        psi_r[:,self.y_t.shape[1]:] = (self.y_t).imag
        chi_r = np.zeros((self.H.n_time,self.y_t.shape[1]*2)).astype(float)
        chi_r[:,:self.y_t.shape[1]] = (self.w_t).real
        chi_r[:,self.y_t.shape[1]:] = (self.w_t).imag
        #TODO: complete vbp sensitivity equation

    def sign_smooth(self,x,e,n=1): #use only if looking for bang-bang and using Lobatto iiiA
        g = lambda x,e : np.exp(e**2/(x**2-e**2))
        C = 4 - 4/np.e #int_-1^1 dx exp(1/(x^2-1))
        if x<e:
            int_approx = lambda x,e: np.trapz(g(np.linspace(x,e-1.e-8,1001),e),x=np.linspace(x,e,1001))
        else:
            int_approx = lambda x,e: np.trapz(g(np.linspace(e+1.e-8,x,1001),e),x=np.linspace(e,x,1001))
        return (1-2*C/e**n * int_approx(x,e))


    def Phi(self,idx):
        return np.array([
            self.H.op.op['S_x'].dot(self.y_t[idx]),
            self.H.op.op['S_y'].dot(self.y_t[idx]),
            self.H.op.op['S_z'].dot(self.y_t[idx])
        ]) #TODO: Check if this is correct
    
    def f(self,t,y,P,Q):
        idx = (t/self.H.dt+1.e-6)
        if abs(idx-int(idx))<2.e-6:
            return P(int(idx),y) + Q(int(idx))
        d = idx-int(idx)
        p1 = P(int(idx),y) + Q(int(idx))
        p2 = P(int(idx+1),y) + Q(int(idx+1))
        return (p1+p2)/2 #self.sign_smooth(d-0.5,1.e-5)*(p1-p2)/2+(p1+p2)/2
