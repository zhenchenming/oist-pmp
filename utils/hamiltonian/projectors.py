import numpy as np
from numpy import kron
from scipy.sparse import csc_matrix

def singProj(sparse=True):
	s_z = np.array([[0.5,0],[0,-0.5]])
	s_plus = np.array([[0,1],[0,0]])
	s_minus = np.array([[0,0],[1,0]])
	
	P_S = 0.25*np.eye(4)- kron( s_z,s_z) - kron(0.5*s_plus,s_minus) \
		- kron(0.5*s_minus,s_plus)
	if sparse:
		return csc_matrix(P_S)
	return P_S

def tripProj(sparse=True):
	s_z = np.array([[0.5, 0], [0, -0.5]])
	s_plus = np.array([[0,1],[0,0]])
	s_minus = np.array([[0,0],[1,0]])
	
	P_T = 0.75*np.eye(4) + kron(s_z,s_z) + kron(0.5*s_plus,s_minus) \
		+ kron(0.5*s_minus,s_plus)
	
	if sparse:
		return csc_matrix(P_T)
	return P_T