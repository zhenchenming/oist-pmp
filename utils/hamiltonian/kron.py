import numpy as np
from scipy.sparse import kron


def kron_(a):
	if type(a[0]) == np.ndarray:
		return kron_array(a)
	else:
		return kron_sparse(a)


def kron_array(a):
	#check if is sparse
	if len(a) > 1:
		return np.kron(kron_array(a[:-1]), a[-1])
	if len(a) == 1:
		return a[0]
	assert(0)

def kron_sparse(a):
	#check if is sparse
	if len(a) > 1:
		return kron(kron_sparse(a[:-1]), a[-1])
	if len(a) == 1:
		return a[0]
	assert(0)