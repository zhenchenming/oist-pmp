import numpy as np
from hamiltonian.projectors import singProj,tripProj
from hamiltonian.spin_operators import spinOperatorX,spinOperatorY,spinOperatorZ
from hamiltonian.kron import kron_
from scipy.sparse import eye

eye_ = lambda x: np.eye(x)#eye(x,format='csc')
spinOperatorX_ = lambda x: spinOperatorX(x,sparse=False)
spinOperatorY_ = lambda x: spinOperatorY(x,sparse=False)
spinOperatorZ_ = lambda x: spinOperatorZ(x,sparse=False)
singProj_ = lambda : singProj(sparse=False)
tripProj_ = lambda : tripProj(sparse=False)


class Hamiltonian:
    def __init__(self,spin_modules,n_time=2000,t_tot=14.,dynamic_hyperfine=True,diffusion=True):
        self.spin_dims = (spin_modules*2+1).astype(int)
        self.n_nuclei = len(spin_modules)
        self.dynamic_hyperfine = dynamic_hyperfine
        self.diffusion = diffusion
        self.anisotropy = False
        self.gamma_e = 176.085963023#176.00596

        self.k_S = 0.5/self.gamma_e
        self.k_T = 0.5/self.gamma_e
        self.k_P = 12.5/self.gamma_e #used if diffusion is True
        self.op=Operators(self.spin_dims)
        self.n_nuclei = self.n_nuclei

        self.n_time = n_time
        self.t_tot = t_tot * self.gamma_e
        self.t = np.linspace(0,self.t_tot,self.n_time)
        self.dt = self.t[1]
        
        self.Z = np.prod(self.spin_dims)

        #diagonal elements of hyperfine tensor
        a = [[-0.234, -0.234, 0.117]]
        a+= [[-0.03, -0.022, 0.688]]
        a+= [[0.328, 0.357, 0.485]]
        a+= [[-0.218, -0.202, -0.054]]
        a+= [[-0.218, -0.202, -0.054]]

        self.A = np.diag(a[0])[None,]
        for i in range(1,self.n_nuclei):
            self.A = np.concatenate((self.A,np.diag(a[i])[None,]),axis=0)

        if dynamic_hyperfine:
            self.A = np.repeat(self.A[None,],self.n_time,axis=0)

        #Observable
        #ESTAS CAMBIANDO EL OPERADOOOOOOR WARDAAAAA
        #self.O = kron_((tripProj_(),eye_(self.Z))).astype(complex)  #PT
        self.O = kron_((singProj_(),eye_(self.Z))).astype(complex) #PS

    def _build_dynamic(self,time_index):

        H = self.omega_t[time_index,0]*self.op.op['S_x'] \
                + self.omega_t[time_index,1]*self.op.op['S_y'] \
                + self.omega_t[time_index,2]*self.op.op['S_z']
        

        #CONSTANT MAGNETIC FIELD
        #H += -50.e-3*self.op.op['S_z']

        if self.anisotropy:
            for i in range(self.n_nuclei):
                for j,d_i in enumerate(['x','y','z']):
                    for k,d_s in enumerate(['x','y','z']):
                        H += self.A[time_index,i,j,k]*self.op.op['I_%i%s'%(i+1,d_i)].dot(self.op.op['S_1%s'%d_s])
        else:
            for i in range(self.n_nuclei):
                H += self.A[time_index,i,0,0]*self.op.op['I_%ix'%(i+1)].dot(self.op.op['S_1x']) \
                    + self.A[time_index,i,1,1]*self.op.op['I_%iy'%(i+1)].dot(self.op.op['S_1y']) \
                    + self.A[time_index,i,2,2]*self.op.op['I_%iz'%(i+1)].dot(self.op.op['S_1z'])
        
        self.K = kron_(( (0.5*self.k_S)*singProj_()+(0.5*self.k_T)*tripProj_(),eye_(self.Z)))
        if self.diffusion:
            self.K += self.k_P*kron_((tripProj_()+singProj_(),eye_(self.Z)))

        self.H_0 = (H - 1.0j*self.K)
        return self.H_0
    
    def _build_static(self,time_index):
        H = self.omega_t[time_index,0]*self.op.op['S_x'] \
            + self.omega_t[time_index,1]*self.op.op['S_y'] \
            + self.omega_t[time_index,2]*self.op.op['S_z']
        
        if self.anisotropy:
            for i in range(self.n_nuclei):
                for j,d_i in enumerate(['x','y','z']):
                    for k,d_s in enumerate(['x','y','z']):
                        H += self.A[i,j,k]*self.op.op['I_%i%s'%(i+1,d_i)].dot(self.op.op['S_1%s'%d_s])
        
        else:
            for i in range(self.n_nuclei):
                H += self.A[i,0,0]*self.op.op['I_%ix'%(i+1)].dot(self.op.op['S_1x']) \
                    + self.A[i,1,1]*self.op.op['I_%iy'%(i+1)].dot(self.op.op['S_1y']) \
                    + self.A[i,2,2]*self.op.op['I_%iz'%(i+1)].dot(self.op.op['S_1z'])
            
        if self.diffusion:
            self.K += self.k_P*kron_((tripProj_()+singProj_(),eye_(self.Z)))
        self.H_0 = H - 1.0j*self.K
        return self.H_0

    def __call__(self,time_index=0):
        if self.dynamic_hyperfine:
            H = self._build_dynamic(time_index)
        else:
            H = self._build_static(time_index)
        return H
    
    def build_H_t(self):
        if self.n_nuclei >4:
            raise ValueError('Nuclei > 5. Memory exceeds when building H_t')
        H = self.omega_t[:,0,None,None]*self.op.op['S_x'][None,:,:] \
            + self.omega_t[:,1,None,None]*self.op.op['S_y'][None,:,:] \
            + self.omega_t[:,2,None,None]*self.op.op['S_z'][None,:,:]
        
        #CONSTANT MAGNETIC FIELD
        #H += -50.e-3*np.ones((self.n_time))[:,None,None]*self.op.op['S_z'][None,:,:]

        if self.anisotropy:
            for i in range(self.n_nuclei):
                for j,d_i in enumerate(['x','y','z']):
                    for k,d_s in enumerate(['x','y','z']):
                        H += self.A[:,i,j,k,None,None]*self.op.op['I_%i%s'%(i+1,d_i)].dot(self.op.op['S_1%s'%d_s])[None,:,:]
        else:
            for i in range(self.n_nuclei):
                H += self.A[:,i,0,0,None,None]*self.op.op['I_%ix'%(i+1)].dot(self.op.op['S_1x'])[None,:,:] \
                    + self.A[:,i,1,1,None,None]*self.op.op['I_%iy'%(i+1)].dot(self.op.op['S_1y'])[None,:,:] \
                    + self.A[:,i,2,2,None,None]*self.op.op['I_%iz'%(i+1)].dot(self.op.op['S_1z'])[None,:,:]
            
        self.K = kron_(( (0.5*self.k_S)*singProj_()+(0.5*self.k_T)*tripProj_(),eye_(self.Z)))
        if self.diffusion:
            self.K += self.k_P*kron_((tripProj_()+singProj_(),eye_(self.Z)))
        self.H_0 = (H - 1.0j*self.K)
        return self.H_0


class Operators:
    def __init__(self, spin_dims):
        n_nuclei = len(spin_dims)
        Z = np.prod(spin_dims)

        self.op = {}

        self.op['S_1x'] = kron_(( spinOperatorX_(2), eye_(2*Z) )).astype(complex)
        self.op['S_1y'] = kron_(( spinOperatorY_(2), eye_(2*Z) )).astype(complex)
        self.op['S_1z'] = kron_(( spinOperatorZ_(2), eye_(2*Z) )).astype(complex)

        self.op['S_2x'] = kron_(( eye_(2), spinOperatorX_(2), eye_(Z) )).astype(complex)
        self.op['S_2y'] = kron_(( eye_(2), spinOperatorY_(2), eye_(Z) )).astype(complex)
        self.op['S_2z'] = kron_(( eye_(2), spinOperatorZ_(2), eye_(Z) )).astype(complex)

        self.op['S_x'] = self.op['S_1x'] + self.op['S_2x']
        self.op['S_y'] = self.op['S_1y'] + self.op['S_2y']
        self.op['S_z'] = self.op['S_1z'] + self.op['S_2z']

        #electron spins
        e_S = eye_(4)

        #nuclear operators

        for i in range(n_nuclei):
            self.op['I_%ix'%(i+1)] = kron_((
                e_S,
                *(eye_(dim) for dim in spin_dims[:i]),
                spinOperatorX_(spin_dims[i]),
                *(eye_(dim) for dim in spin_dims[i+1:])
            ))

            self.op['I_%iy'%(i+1)] = kron_((
                e_S,
                *(eye_(dim) for dim in spin_dims[:i]),
                spinOperatorY_(spin_dims[i]),
                *(eye_(dim) for dim in spin_dims[i+1:])
            ))

            self.op['I_%iz'%(i+1)] = kron_((
                e_S,
                *(eye_(dim) for dim in spin_dims[:i]),
                spinOperatorZ_(spin_dims[i]),
                *(eye_(dim) for dim in spin_dims[i+1:])
            ))
