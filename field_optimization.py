import datetime
import os
import sys
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
sys.path.append('utils')
from utils.hamiltonian.kron import kron_
from utils.hamiltonian.hamiltonian import Hamiltonian
from utils.ode.schrodinger import H_ODE
from utils.optimization.GPM import GPM
from utils.optimization.Gramm import Gramm
from utils.optimization.metrics import fraction_yield,quantum_yield,integrated_yield,ensemble_yields,J2
plt.ion()
# ---------------------------- system construction --------------------------- #

#spin modules
s = np.array([1/2,1/2])
#diffusion term added if True, n_time: mesh points, t_tot: final moment T in microseconds
H = Hamiltonian(s,diffusion=True,n_time=1000,t_tot=.5)
#rates
H.k_S = 10./H.gamma_e
H.k_T = 10./H.gamma_e
H.k_P = 12.5/H.gamma_e

# --------------------------- ensemble construction -------------------------- #

#Electron spin eigenstates
#singlet
psi_S = np.array([0.0, 1/np.sqrt(2), -1/np.sqrt(2), 0.0])[:, None]
#triplet
psi_T_plus = np.array([1.0, 0.0, 0.0, 0.0])[:, None]
psi_T_0 = np.array([0.0, 1.0/np.sqrt(2.0), 1.0/np.sqrt(2.0), 0.0])[:, None]
psi_T_minus = np.array([0.0, 0.0, 0.0, 1.])[:, None]

#Nuclear spins eigenstates. Uncloupled -> state i = (0....,1,0....) where 1 is in the i-th position
#Compressed in eye
psi_0_nuc = np.eye(H.Z)

# ------------------------------ initial states ------------------------------ #

if triplet:=True:
    psi_0 = [
        kron_((psi_T_plus, psi_0_nuc[i,:,None])).astype(complex).flatten()
        for i in range(H.Z)
    ]
    psi_0 += [
        kron_((psi_T_0, psi_0_nuc[i,:,None])).astype(complex).flatten()
        for i in range(H.Z)
    ]
    psi_0 += [
        kron_((psi_T_minus, psi_0_nuc[i,:,None])).astype(complex).flatten()
        for i in range(H.Z)
    ]
else:
    psi_0 = [
        kron_((psi_S, psi_0_nuc[i,:,None])).astype(complex).flatten()
        for i in range(H.Z)
    ]

# -------------------------- set learning parameters ------------------------- #

#ODE solver (forward and backward)
ode = H_ODE(H)
#system bounds
m_u = np.array([3,3,-1])[None,:]*1.e-3
M_u = np.array([6,6,2])[None,:]*1.e-3
bounds_u = (m_u,M_u) #mT
#initial controls
field = np.ones((H.n_time, 3))*M_u #all in upper bound
H.omega_t = field

H.A[:,0,0,0] = -0.234#bounds_A[0]
H.A[:,0,1,1] = -0.234#bounds_A[0]
H.A[:,0,2,2] = 0.117#bounds_A[0]

#initial values
initial_f_y,initial_i_y,initial_q_y,_ = ensemble_yields(psi_0,ode,H,method='rk4',build_Ht=True)
initial_u = H.omega_t.copy()
#regularization
th = 0
tol= 0#1.e-5
#GPM
gpm = GPM(H,ode,psi_0)

# -------------------------- hyperfine optimization -------------------------- #
if field_opt:=True:
    print('Initial:',initial_q_y)
    print('---------------')
    epochs = [50,50,100,400,100] #checkpoints
    #learning rate
    lr = 2.e1
    #optimization
    J_gpm0,f_y_gpm,i_y_gpm,g_gpm0 = gpm.field_optimization(lr,0.,epochs[0],tol,update_lr=0,bounds=bounds_u,maximize=True,check_bvp=00)
    u_gpm0 = H.omega_t.copy()
    J_gpm1,f_y_gpm,i_y_gpm,g_gpm1 = gpm.field_optimization(lr,0.,epochs[1],tol,update_lr=0,bounds=bounds_u,maximize=True,check_bvp=00)
    u_gpm1 = H.omega_t.copy()
    J_gpm2,f_y_gpm,i_y_gpm,g_gpm2 = gpm.field_optimization(lr,0.,epochs[2],tol,update_lr=0,bounds=bounds_u,maximize=True,check_bvp=00)
    u_gpm2 = H.omega_t.copy()
    J_gpm3,f_y_gpm,i_y_gpm,g_gpm3 = gpm.field_optimization(lr,0.,epochs[3],tol,update_lr=0,bounds=bounds_u,maximize=True,check_bvp=00)
    u_gpm3 = H.omega_t.copy()
    J_gpm4,f_y_gpm,i_y_gpm,g_gpm4 = gpm.field_optimization(lr,0.,epochs[4],tol,update_lr=0,bounds=bounds_u,maximize=True,check_bvp=00)
    u_gpm4 = H.omega_t.copy()
    A_gpm4 = H.A.copy()

    J_gpm = np.concatenate((J_gpm0[:-1],J_gpm1[:-1],J_gpm2[:-1],J_gpm3[:-1],J_gpm4[:-1]))


# ------------------------------- save results ------------------------------- #

H.A = A_gpm4.copy()
H.omega_t = u_gpm4.copy()

es = len(psi_0)
ws = len(psi_0[0])


if save_wave:=True:
    wave = np.zeros((H.n_time,ws*es*4))
    for i,psi_ in enumerate(psi_0):
        ode.set_y(psi_)
        ode.solve_forward(method='rk4',build_Ht=True)
        ode.solve_backward(method='rk4')
        wave[:,ws*i:ws*(i+1)] = ode.y_t.real
        wave[:,ws*es+(ws*i):ws*es+(ws*(i+1))] = ode.y_t.imag
        wave[:,2*ws*es+(ws*i):2*ws*es+(ws*(i+1))] = ode.w_t.real
        wave[:,3*ws*es+(ws*i):3*ws*es+(ws*(i+1))] = ode.w_t.imag

    #change filename if wanted
    np.savetxt('wave_%ip_optimization_.dat'%H.n_nuclei,wave)
    np.save('A_%ip_optimization_.npy'%H.n_nuclei,H.A)
    np.save('u_%ip_optimization_.npy'%H.n_nuclei,H.omega_t)


# -------------------------- plot results (optional) ------------------------- #
"""
if load_data:=False:
    A__ = np.load('data4jose/_A_1p_simultaneous_optimization.npy')
    u__ = np.load('data4jose/_u_1p_simultaneous_optimization.npy')
    filename = 'data_jose/1P_MAX_T_1e-06_1000.mat'
    mat = scipy.io.loadmat(filename)
    u_ =  mat['sol_PMP'][0][0][7][0][0][0][0][0].T[::2]
    A_ = H.A*0
    aux = mat['sol_PMP'][0][0][7][0][0][1][0][0].T
    A_[:,0,0,0] = aux[::2,0]
    A_[:,0,1,1] = aux[::2,1]
    A_[:,0,2,2] = aux[::2,2]
else:
    A_ = A_gpm4.copy()
    u_ = u_gpm4.copy()

H.A = A_.copy()
H.omega_t = u_.copy()

_,_,_,g_u,g_A=gpm.optimization((5.e-11,5.e-11),(0.,0.),1,tol,update_lr=0,bounds=(bounds_u,bounds_A),maximize=True)

fig = plt.figure(figsize=(14,6))
fig2 = plt.figure(figsize=(14,6))
fig3 = plt.figure(figsize=(14,6))
fig4 = plt.figure(figsize=(14,6))
for i in range(3):
    ax=fig.add_subplot(3,1,i+1)
    ax2=fig2.add_subplot(3,1,i+1)
    ax3=fig3.add_subplot(3,1,i+1)
    ax4=fig4.add_subplot(3,1,i+1)
    ax.plot(H.t/H.gamma_e,H.A[:,0,i,i],color='red',alpha=0.6)
    ax2.plot(H.t/H.gamma_e,g_A[:,0,i,i],color='red')
    ax2.axhline(0.,color='black',linestyle='--')
    ax3.plot(H.t/H.gamma_e,u_[:,i]*1.e3,color='red',alpha=0.6)
    ax4.plot(H.t/H.gamma_e,g_u[:,i],color='red')
    ax4.axhline(0.,color='black',linestyle='--')
"""